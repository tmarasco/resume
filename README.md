# Resume

[![Build Status](https://gitlab.com/cc4triose/resume/badges/master/build.svg)](https://gitlab.com/cc4triose/resume)


### *A responsive resume/cv site generator for GitLab Pages. Powered by Mustache templates engine & GitLab CI.*

## Quick Start 🚀

1. Fork this repository. (Don't forget to remove the fork before making changes!) 

2. Modify **data.json** with your information. 

3. Push your changes to gitlab. 

Now your resume is public on https://{username}.gitlab.io/resume/ 
Go here for an example of a resume/portfolio: [https://cc4triose.gitlab.io/resume/](https://cc4triose.gitlab.io/resume/)

## Customize your resume

To add your style just modify the style.css file in the public folder or the mustache template file to change the layout.


## License©️

Default theme designed by [Franklin Schamhart](https://dribbble.com/shots/1887983-Resume).
Forked from [https://luisfuentes.gitlab.io/resume/](https://luisfuentes.gitlab.io/resume/)

Resume is licensed under the MIT Open Source license. For more information, see the LICENSE file in this repository.